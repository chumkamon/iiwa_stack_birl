#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/birl/iiwa-stack/devel/.private/catkin_tools_prebuild:$CMAKE_PREFIX_PATH"
export LD_LIBRARY_PATH="/home/birl/worksp/ros_realsense2/devel/lib:/opt/ros/kinetic/lib:/opt/ros/kinetic/lib/x86_64-linux-gnu:/usr/local/cuda-9.0/lib64:/usr/local/lib/nuitrack:/usr/local/cuda/lib64:/usr/local/cuda/extras/CUPTI/lib64:/usr/lib/nvidia-384"
export PATH="/opt/ros/kinetic/bin:/usr/local/cuda-9.0/bin:/home/birl/bin:/home/birl/.local/bin:/home/birl/bin:/home/birl/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin:/usr/local/cuda/bin"
export PKG_CONFIG_PATH="/home/birl/worksp/ros_realsense2/devel/lib/pkgconfig:/opt/ros/kinetic/lib/pkgconfig:/opt/ros/kinetic/lib/x86_64-linux-gnu/pkgconfig"
export PWD="/home/birl/iiwa-stack/build/catkin_tools_prebuild"
export PYTHONPATH="/home/birl/worksp/ros_realsense2/devel/lib/python2.7/dist-packages:/opt/ros/kinetic/lib/python2.7/dist-packages:/usr/local/lib"
export ROSLISP_PACKAGE_DIRECTORIES="/home/birl/iiwa-stack/devel/.private/catkin_tools_prebuild/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/birl/iiwa-stack/build/catkin_tools_prebuild:/home/birl/worksp/ros_realsense2/src:/home/birl/iiwa-stack/src/force_torque_tools/force_torque_sensor_calib:/home/birl/iiwa-stack/src/force_torque_tools/force_torque_tools:/home/birl/iiwa-stack/src/force_torque_tools/gravity_compensation:/home/birl/iiwa-stack/src/iiwa:/home/birl/iiwa-stack/src/iiwa_control:/home/birl/iiwa-stack/src/iiwa_control_pos:/home/birl/iiwa-stack/src/iiwa_description:/home/birl/iiwa-stack/src/iiwa_force_control:/home/birl/iiwa-stack/src/iiwa_gazebo:/home/birl/iiwa-stack/src/iiwa_moveit:/home/birl/iiwa-stack/src/iiwa_msgs:/home/birl/iiwa-stack/src/iiwa_nodetest:/home/birl/iiwa-stack/src/iiwa_ros:/home/birl/iiwa-stack/src/iiwa_hw:/home/birl/iiwa-stack/src/trajectory_planning/time_optimal_trajectory_generation:/home/birl/iiwa-stack/src/timesync_ros:/home/birl/iiwa-stack/src/lpms_imu:/opt/ros/kinetic/share"