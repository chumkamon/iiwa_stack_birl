# CMake generated Testfile for 
# Source directory: /home/birl/iiwa-stack/src
# Build directory: /home/birl/iiwa-stack/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(gtest)
subdirs(force_torque_tools/force_torque_tools)
subdirs(iiwa)
subdirs(iiwa_control)
subdirs(iiwa_description)
subdirs(iiwa_gazebo)
subdirs(iiwa_moveit)
subdirs(iiwa_msgs)
subdirs(trajectory_planning/time_optimal_trajectory_generation)
subdirs(iiwa_nodetest)
subdirs(iiwa_ros)
subdirs(force_torque_tools/gravity_compensation)
subdirs(timesync_ros)
subdirs(lpms_imu)
subdirs(iiwa_hw)
subdirs(force_torque_tools/force_torque_sensor_calib)
subdirs(iiwa_control_pos)
subdirs(iiwa_force_control)
