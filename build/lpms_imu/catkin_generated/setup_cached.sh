#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/birl/iiwa-stack/devel/.private/lpms_imu:$CMAKE_PREFIX_PATH"
export PWD="/home/birl/iiwa-stack/build/lpms_imu"
export ROSLISP_PACKAGE_DIRECTORIES="/home/birl/iiwa-stack/devel/.private/lpms_imu/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/birl/iiwa-stack/src/lpms_imu:$ROS_PACKAGE_PATH"