# generated from catkin/cmake/em/order_packages.cmake.em

set(CATKIN_ORDERED_PACKAGES "")
set(CATKIN_ORDERED_PACKAGE_PATHS "")
set(CATKIN_ORDERED_PACKAGES_IS_META "")
set(CATKIN_ORDERED_PACKAGES_BUILD_TYPE "")
list(APPEND CATKIN_ORDERED_PACKAGES "force_torque_tools")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "force_torque_tools/force_torque_tools")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "True")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "iiwa")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "iiwa")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "True")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "iiwa_control")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "iiwa_control")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "iiwa_description")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "iiwa_description")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "iiwa_gazebo")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "iiwa_gazebo")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "iiwa_moveit")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "iiwa_moveit")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "iiwa_msgs")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "iiwa_msgs")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "time_optimal_trajectory_generation")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "trajectory_planning/time_optimal_trajectory_generation")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "iiwa_nodetest")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "iiwa_nodetest")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "iiwa_ros")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "iiwa_ros")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "gravity_compensation")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "force_torque_tools/gravity_compensation")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "timesync")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "timesync_ros")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "lpms_imu")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "lpms_imu")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "iiwa_hw")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "iiwa_hw")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "force_torque_sensor_calib")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "force_torque_tools/force_torque_sensor_calib")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "iiwa_control_pos")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "iiwa_control_pos")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "iiwa_force_control")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "iiwa_force_control")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")

set(CATKIN_MESSAGE_GENERATORS )

set(CATKIN_METAPACKAGE_CMAKE_TEMPLATE "/usr/lib/python2.7/dist-packages/catkin_pkg/templates/metapackage.cmake.in")
